import scrapy
from datetime import datetime
import array
import json
from scrapy import Selector

from tutorial.items import Retweet

replies = []

class QuotesSpider(scrapy.Spider):
    name = "quotes"

    def start_requests(self):
        urls = [
            'https://twitter.com/jokowi',
            'https://twitter.com/aniesbaswedan',
            'https://twitter.com/basuki_btp',
            'https://twitter.com/ChrisEvans',
            'https://twitter.com/RobertDowneyJr'
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        page = response.url.split('/')[-1]
        filename = 'parent_%s.txt' % page

        with open(filename, 'w+') as f:
            usernames = response.css('.username b ::text').getall()
            dates = response.xpath("//div[@class='stream-item-header']/small[@class='time']/a/span/@data-time").getall()
            tweets = response.xpath('.//div[@class="js-tweet-text-container"]')

            ids = response.xpath('.//@data-tweet-id').extract()

            row = []

            for index in range(len(usernames)):
                if usernames[index]:
                    name = usernames[index]
                else:
                    name = ""

                if index < len(ids):
                    _id = ids[index]
                    url = "https://api.twitter.com/2/timeline/retweeted_by.json?tweet_id=1255453835495161858                                                                "+_id
                    yield scrapy.Request(url=url, callback=self.parseRetweet)
                else:
                    _id = ""
                        
                if index < len(dates):
                    date = datetime.fromtimestamp(int(dates[index])).strftime('%Y-%m-%d %H:%M:%S')
                else:
                    date = ""

                if index < len(tweets):
                    tweet = tweets[index].xpath('string(./p//text())').get()
                else:
                    tweet = ""
                        
                if date != "" or tweet != "":
                    row.append({
                        'tweet_id':_id,
                        'usernames':name,
                        'time':date,
                        'tweet':tweet
                    })
                
            json.dump(row, f)

        self.log('Saved file %s' % filename)

    def parseRetweet(self,response):
        url = response.url.split("id=")
        filename = 'retweet-%s.txt' % url[1]
        with open(filename, 'wb') as f:
            jsonData = json.loads(response.body)

            url = response.url.split("id=")

            html = jsonData['htmlUsers']
            
            sel = Selector(text=html)
            
            parent_id = url[1]
            usernames = sel.css('.username b ::text').getall()
            tweets = sel.xpath('string(//div[@class=" content"]/p//text())').getall()

            tweet_data = ""

            for tweet in tweets:
                tweetw = tweet.replace("\n", "")
                tweetw = tweetw.replace('&nbsp;','')
                if tweetw != "" or tweetw != " " or tweetw == None:
                    tweet_data = tweet
        
            data = []

            data.append({
                'parent_id':parent_id,
                'reply_username':usernames[0],
                'reply_tweet':tweet_data
            })

            json.dump(data,f)