import json
import array
import csv

urls = [
    'https://twitter.com/jokowi',
    'https://twitter.com/aniesbaswedan',
    'https://twitter.com/basuki_btp',
    'https://twitter.com/ChrisEvans',
    'https://twitter.com/RobertDowneyJr'
]

getData = []

for url in urls:
    
    filename = url.split('/')[-1]
    filename = "parent_"+filename+".txt"

    with open(filename,'r+') as file_data:
        parent_data = json.load(file_data)
        
        for data in parent_data:

            tweet_usernames     = data['usernames']
            tweet_tweet         = data['tweet']
            tweet_time          = data['time']
            tweet_retwet       = ""
            tweet_retweet_user  = ""

            with open("retweet-"+data['tweet_id']+'.txt','r+') as retweet_data:
                retweet_item = json.load(retweet_data)
                
                tweet_retweet_user  = retweet_item[0]['reply_username']
                tweet_retweet       = retweet_item[0]['reply_tweet']

            getData.append({
                'tweet_usernames':tweet_usernames,
                'tweet_tweet':tweet_tweet,
                'tweet_time':tweet_time,
                'tweet_retweet':tweet_retweet,
                'tweet_retweet_time':tweet_time,
                'tweet_retweet_user':tweet_retweet_user,
            })

with open('hasil.html','wb') as hasil:

    row = []

    for result in getData:
        row_data = "<tr><td>"+result['tweet_time']+"</td>"+"<td>"+result['tweet_usernames']+"</td>"+"<td>"+result['tweet_tweet']+"</td>"+"<td>"+result['tweet_retweet_user']+"</td>"+"<td>"+result['tweet_retweet']+"</td></tr>"
        row.append(row_data)

    row_result = "".join(row)

    header = "<table border='1' style='border-collapse: collapse;' width='100%' ><tr><th>Time</th><th>Authors</th><th>Tweets</th><th>Retweet Author</th><th>Retweet Content</th></tr>"
    table = header+row_result+"</table>"

    hasil.write(table.encode('utf8'))   

# Open/create a file to append data to
csvFile = open('result.csv', 'wb')

#Use csv writer
csvWriter = csv.writer(csvFile)
csvWriter.writerow([
    "Tanggal dan waktu",
    "Nama Penulis",
    "Cuitan Penulis",
    "Tanggal dan waktu pembalas",
    "Pembalas",
    "Cuitan Pembalas"
])

for result in getData:
    csvWriter.writerow([
        result['tweet_time'],
        result['tweet_usernames'].encode('utf8'),
        result['tweet_tweet'].encode('utf8'),
        result['tweet_retweet_time'].encode('utf8'),
        result['tweet_retweet_user'].encode('utf8'),
        result['tweet_retweet'].encode('utf8'),
    ])
csvFile.close()